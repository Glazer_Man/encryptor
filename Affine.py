def Affine_Encrypt(str1, a, b):
    try:
        str1 = str(str1)
        a = int(a)
        b = int(b)
    except ValueError:
        return "invalid a/b value please enter an integer"
    Check_a = False
    a_Pos = [1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25]
    for i in a_Pos:
        if i == a:
            Check_a = True
    ascii_A = ord('A')
    ascii_Z = ord('Z')
    ascii_a = ord('a')
    ascii_z = ord('z')
    new_str = ""
    if b not in range(0, 26):
        return "invalid b value please enter a number between 0 - 25"
    if Check_a:
        for current_char in str1:
            current_char = ord(current_char)
            if current_char in range(ascii_a, ascii_z + 1):
                current_char = current_char - ascii_a
                current_char = (a * current_char + b) % 26
                current_char = current_char + ascii_a
            elif current_char in range(ascii_A, ascii_Z + 1):
                current_char = current_char - ascii_A
                current_char = (a * current_char + b) % 26
                current_char = current_char + ascii_A
            current_char = chr(current_char)
            new_str = new_str + current_char
        return new_str
    else:
        return "invalid a value please enter a number from this list --> [1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25]"

