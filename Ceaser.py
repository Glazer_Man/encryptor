def Ceaser_Encrypt(str1, jump):
    try:
        jump = int(jump)
    except ValueError:
        return "invalid value please enter an integer"
    if(jump > 26):
        jump = (jump % 26) 
    str1 = str(str1)
    ascii_A = ord('A')
    ascii_Z = ord('Z')
    ascii_a = ord('a')
    ascii_z = ord('z')
    ascii_C_Char = 0
    new_str = ""
    diff = 0
    for current_char in str1:
        ascii_C_Char = ord(current_char)
        if ascii_C_Char in range(ascii_a, ascii_z + 1):
            if (ascii_C_Char + jump) in range(ascii_a, ascii_z + 1):
                current_char = chr(ascii_C_Char + jump) 
            else:
                diff = (ascii_C_Char + jump) - ascii_z - 1
                current_char = chr(ascii_a + diff)
        elif ascii_C_Char in range(ascii_A, ascii_Z + 1):
            if (ascii_C_Char + jump) in range(ascii_A, ascii_Z + 1):
                current_char = chr(ascii_C_Char + jump)
            else:
                diff = (ascii_C_Char + jump) - ascii_Z - 1
                current_char = chr(ascii_A + diff)
        new_str += current_char
    return new_str

