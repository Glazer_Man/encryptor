from flask import Flask, request, render_template
from Ceaser import *
from ROT_13 import *
from Keyword import *
from Zigzag import *
from Affine import *

app = Flask(__name__)


def ATBASH_encrypt(x):
	x = str(x)
	new_str = ""
	for current_char in x:
		if ord(current_char) in range(ord('a'), ord('z') + 1):
			new_str += chr(ord('a') + ord('z') - ord(current_char))
		elif ord(current_char) in range(ord('A'), ord('Z') + 1):
			new_str += chr(ord('A') + ord('Z') - ord(current_char))
		else:
			new_str += current_char
	return new_str


@app.route("/")
def main():
	return render_template("Main_page.html")



@app.route("/index1", methods=['GET'])
def index1():
	return render_template("index.html")


@app.route("/atbash", methods=['POST'])
def atbash():
	encrypted = ATBASH_encrypt(request.form["text"])
	return render_template("response.html", encrypted=encrypted, type="atbash")

@app.route("/index2", methods=['GET'])
def index2():
	return render_template("Rot_13_index.html")


@app.route("/rot13", methods=['POST'])
def rot13():
	encrypted = ROT13_Encrypt(request.form["text"])
	return render_template("response.html", encrypted=encrypted, type="rot13")

@app.route("/index3", methods=['GET'])
def index3():
	return render_template("Ceaser_index.html")

@app.route("/ceaser", methods=['POST'])
def ceaser():
	encrypted = Ceaser_Encrypt(request.form["text[0]"] , request.form["text[1]"])
	return render_template("response.html", encrypted=encrypted, type="ceaser")

@app.route("/index4", methods=['GET'])
def index4():
	return render_template("Keyword_index.html")

@app.route("/keyword", methods=['POST'])
def keyword():
	encrypted = keyword_encrypt(request.form["text"] , request.form["key"])
	return render_template("response.html", encrypted=encrypted, type="keyword")

@app.route("/index5", methods=['GET'])
def index5():
	return render_template("Zigzag_index.html")

@app.route("/zigzag", methods=['POST'])
def zigzag():
	encrypted = zigzag_encrypt(request.form["text"] , request.form["key"])
	return render_template("response.html", encrypted=encrypted, type="zigzag")

@app.route("/index6", methods=['GET'])
def index6():
	return render_template("Affine_index.html")

@app.route("/affine", methods=['POST'])
def affine():
	encrypted = Affine_Encrypt(request.form["text"] , request.form["a"], request.form["b"])
	return render_template("response.html", encrypted=encrypted, type="affine")

if __name__ == '__main__':
	app.run(debug=True)
