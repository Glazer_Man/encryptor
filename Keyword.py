def keyword_encrypt(plain_text, key):
    try:                    #--> at first we need to covert the plain_text and the key values to a string type
        plain_text = str(plain_text)            
        key = str(key)          
    except ValueError:
        return "invalid values"
    flag1, flag2 = False, False
    for check1 in key:      #--> we need to check if the key has two characters from the same type
        for check2 in key:
            if flag1:
                if check1 == check2:
                    flag2 = True
            if check1 == check2:
                    flag1 = True
            if flag1 and flag2:
                return "the key has two characters from the same type"
        flag1 = False
        flag2 = False
    plain_text_dictionary = [
        'a', 'b', 'c',
        'd', 'e', 'f',
        'g', 'h', 'i',
        'j', 'k', 'l',
        'm', 'n', 'o',
        'p', 'q', 'r',
        's', 't', 'u',
        'v', 'w', 'x',
        'y', 'z'
        ]
    
    for current_char in key: #the plain_text_dictionary contains the full abc before we change it to the chiper_text_dictionary abc with the key inside it
        for j in range(len(plain_text_dictionary) ):  #--> this for loop change the characters from the key string to '0' so we can identify the keyword charcter that are missing in the abc later
            if plain_text_dictionary[j] == current_char:
                plain_text_dictionary[j] = '0'
    chiper_text_dictionary = []
    
    for j in range(len(plain_text_dictionary)): #--> we creating a new list with out the characters from the key string
        if plain_text_dictionary[j] != '0':
            chiper_text_dictionary += plain_text_dictionary[j]
    new_chiper_text_dictionary = []
    
    for current_char in key:
        new_chiper_text_dictionary += current_char
    new_chiper_text_dictionary += chiper_text_dictionary
    new_str = ""
    
    for current_char in plain_text:
        if ord(current_char) in range(ord('a'), ord('z') + 1):
            x = ord(current_char) - ord('a')
            new_str += new_chiper_text_dictionary[x]
        elif ord(current_char) in range(ord('A'), ord('Z') + 1):
            x = ord(current_char) - ord('A')
            new_str += new_chiper_text_dictionary[x]
        else:
            new_str += current_char
    return new_str

