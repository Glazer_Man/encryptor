def zigzag_encrypt(plain_text, key):
    try:
        plain_text = str(plain_text)            
        key = int(key)          
    except ValueError:
        return "invalid values"

    matrix_width, matrix_height = len(plain_text), key
    matrix = [['`' for x in range(matrix_width)] for y in range(matrix_height)]
    new_str = ""
    i, j = 0, 0 
    for current_char in plain_text:
        if i < 0:
            matrix [-i][j] = current_char
        else:
            matrix [i][j] = current_char
        if i == matrix_height - 1:
            i = -matrix_height + 1    
        i += 1
        j += 1
    for y in range(0, matrix_height):
        for x in range(0, matrix_width):
            if ord(matrix[y][x]) != ord('`'):
                new_str += matrix[y][x]
    
    return new_str

